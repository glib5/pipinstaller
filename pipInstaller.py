import os
from tkinter.messagebox import askyesno
from tkinter.filedialog import askopenfilename

# =============================================================================

def pip_install():
    """pip-installs all packages in file.txt"""
    # individuate file
    file = askopenfilename()
    assert file[-4:] == ".txt"
    # get list of packages to pip-install
    with open(file) as f:
        packages = f.read().split("\n")
    print(f"Loaded file: {file}")
    print("Loaded package names:")
    for p in packages:
        print(f"\t{p}")
    # start installing
    if askyesno(" ", "Start loading ? "):
        for p in packages:
            print("\n\n\n")
            print(f" *** Installing {p} *** \n")
            os.system(f"pip install {p}")
        print("\nProgram ended")
    else:
        print("\nClosing without installing")
    return

# =============================================================================

if __name__=="__main__":
   pip_install()
   input()
